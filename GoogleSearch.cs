using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace googlesearch
{
    class GoogleSearch
    {
        private static readonly HttpClient client = new HttpClient();
        const string BASE_URI = "https://www.google.com/search?q=";

        static async Task Main(string[] args)
        {
            string uri = BASE_URI + GetSearch("Please enter your search term: ");

            var task = await Task.Factory.StartNew(async () =>
            {
                HttpResponseMessage response = await client.GetAsync(uri);
                var content = await response.Content.ReadAsStringAsync();
                return content;
            });
            Console.WriteLine(await task);
            Console.WriteLine("Do you want to continue? (Y/N): ");
            string answerQuestion = Console.ReadLine();
            if (answerQuestion == "Y")
            {
                await Main(args);
            }
        }
        public static string GetSearch(string message)
        {
            Console.Write(message);
            string query = Console.ReadLine();
            if (query.Length == 0)
            {
                Console.WriteLine("No search term inserted");
                return GetSearch(message);
            }
            return query;
        }
    }
}